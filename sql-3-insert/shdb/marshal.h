#pragma once

#include "row.h"
#include "schema.h"

#include <memory>

namespace shdb {

class Marshal
{
    std::shared_ptr<Schema> schema;
    size_t fixed_row_space;

    size_t calculate_fixed_row_space(uint64_t nulls) const;
    uint64_t get_nulls(const Row &row) const;

public:
    explicit Marshal(std::shared_ptr<Schema> schema);

    size_t get_fixed_row_space() const;
    size_t get_row_space(const Row &row) const;
    void serialize_row(uint8_t *data, const Row &row) const;
    Row deserialize_row(uint8_t *data) const;
};

}    // namespace shdb
