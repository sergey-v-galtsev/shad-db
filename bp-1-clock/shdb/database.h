#pragma once

#include "schema.h"
#include "statistics.h"
#include "store.h"
#include "table.h"

#include <filesystem>

namespace shdb {

class Database
{
    std::shared_ptr<Statistics> statistics;
    std::shared_ptr<Store> store;

    PageProvider create_page_provider(std::shared_ptr<Schema> schema);

public:
    Database(const std::filesystem::path &path, FrameIndex frame_count);

    void create_table(const std::filesystem::path &name, std::shared_ptr<Schema> schema);
    std::shared_ptr<Table> get_table(const std::filesystem::path &name, std::shared_ptr<Schema> schema = nullptr);
    bool check_table_exists(const std::filesystem::path &name);
    void drop_table(const std::filesystem::path &name);
    std::shared_ptr<Statistics> get_statistics();
};

std::shared_ptr<Database> connect(const std::filesystem::path &path, FrameIndex frame_count);

}    // namespace shdb
